import { api } from "./api.js";

export const getMergeRequestsData = async (result, TCMembers, projects) => {
  const openedMergeRequestsInTC = [];
  const mergedMergeRequestsInTC = [];
  const closedMergeRequestsInTC = [];

  console.log("Fetching Technical Committee merge requests...");
  for (const project of projects) {
    const openedMRs = await api.MergeRequests.all({
      projectId: project.id,
      state: "opened",
    });
    openedMergeRequestsInTC.push(...openedMRs);

    const mergedMRs = await api.MergeRequests.all({
      projectId: project.id,
      state: "merged",
    });
    mergedMergeRequestsInTC.push(...mergedMRs);

    const closedMRs = await api.MergeRequests.all({
      projectId: project.id,
      state: "closed",
    });
    closedMergeRequestsInTC.push(...closedMRs);
  }

  const allTCMrs = [
    ...openedMergeRequestsInTC,
    ...mergedMergeRequestsInTC,
    ...closedMergeRequestsInTC,
  ];

  const openedMRsAuthors = openedMergeRequestsInTC.map((mr) => mr.author.name);
  const mergedMRsAuthors = mergedMergeRequestsInTC.map((mr) => mr.author.name);
  const closedMRsAuthors = closedMergeRequestsInTC.map((mr) => mr.author.name);

  for (const member of TCMembers) {
    if (!result[member]) {
      result[member] = {};
    }

    result[member].mergeRequests = {
      opened: openedMRsAuthors.filter((author) => author === member).length,
      merged: mergedMRsAuthors.filter((author) => author === member).length,
      closed: closedMRsAuthors.filter((author) => author === member).length,
    };

    result[member].mergeRequests.total =
      result[member].mergeRequests.opened +
      result[member].mergeRequests.merged +
      result[member].mergeRequests.closed;
  }

  return { allTCMergeRequests: allTCMrs };
};
