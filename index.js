import fs from "fs";
import { getMergeRequestsData } from "./getMergeRequestsData.js";
import { api } from "./api.js";
import { getIssuesData } from "./getIssuesData.js";
import { getDiscussionsData } from "./getDiscussionsData.js";
import { calculateScores } from "./calculateScores.js";

const TC_ID = 9381730;
const result = {};

try {
  // GET TC data
  console.log("Fetching Technical Committee data...");
  const tcData = await api.Groups.show(TC_ID);
  const { projects: TCProjects } = tcData;

  console.log("Fetching members of Technical Committee...");
  const TCMembers = (await api.GroupMembers.all(TC_ID)).map(
    (member) => member.name
  );

  const { allTCMergeRequests } = await getMergeRequestsData(
    result,
    TCMembers,
    TCProjects
  );

  await getIssuesData(result, TCMembers, 9381730);

  await getDiscussionsData(result, allTCMergeRequests, TCMembers);

  calculateScores(result, TCMembers);

  fs.writeFileSync("result.json", JSON.stringify(result, null, 2));
  console.log("Data saved in result.json");
} catch (error) {
  console.error(error);
}
