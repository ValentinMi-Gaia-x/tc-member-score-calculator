# Technical Committee members score calculator

Need node >= 16.16.0

## Install
```
yarn install
```

```
cp .env.example .env
```

Fill .env file with your gitlab token

## Run
`node index.js`

## Parameters 
You wil find a scale object in `calculateScores.js` to ajust the score value of each KPI

## Output
Everything is saved in result.json

Open `index.html` to see the result through UI to sort the data 