import json from "../result.json" assert { type: "json"}

const data = Object.entries(json).map(([name, kpis]) => {
    return {...kpis, name}
}).sort((a, b) => b.score - a.score )

const grid = document.querySelector(".grid");

function createCard(member){
    const htmlCardTemplate =`
    <div class="col-4">
        <div class="card m-3" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">${member.name}</h5>
                <p>MR open: ${member.mergeRequests.opened}</p>
                <p>MR closed: ${member.mergeRequests.closed}</p>
                <p>MR merged: ${member.mergeRequests.merged}</p>
                <p>Issues opened: ${member.issues.opened}</p>
                <p>Comments: ${member.comments}</p>
                <p>Approvals: ${member.approvals}</p>
                <p style="font-weight: bold">Score: ${member.score}</p>
            </div>
        </div>
    </div>
    `

    return htmlCardTemplate
}

for (const member of data) {
    const memberCardHTML = createCard(member)

    grid.insertAdjacentHTML("beforeend", memberCardHTML)
}


