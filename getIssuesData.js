import { api } from "./api.js";

export const getIssuesData = async (result, TCMembers, TCGroupId) => {
  const issues = await api.Issues.all({ groupId: TCGroupId });
  const openedIssuesAuthors = issues.map((issue) => issue.author.name);

  for (const member of TCMembers) {
    if (!result[member]) {
      result[member] = {};
    }
    result[member].issues = {
      opened: openedIssuesAuthors.filter((author) => author === member).length,
    };
  }
};
