export const scale = {
  mergeRequests: 1,
  issues: 1,
  comments: 1,
  approvals: 1,
};

export const calculateScores = (result, TCMembers) => {
  console.log("Calculating members scores...");
  for (const member of TCMembers) {
    let memberScore = 0;

    memberScore += result[member].mergeRequests.total * scale.mergeRequests;
    memberScore += result[member].issues.opened * scale.issues;
    memberScore += result[member].comments * scale.comments;
    memberScore += result[member].approvals * scale.approvals;

    result[member].score = memberScore;
  }
};
