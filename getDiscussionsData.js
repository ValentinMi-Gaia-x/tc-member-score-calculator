import { api } from "./api.js";

export const getDiscussionsData = async (result, mergeRequests, TCMembers) => {
  const mergeRequestsNotes = [];

  console.log("Fetching Technical Committee discussions...");
  (
    await Promise.all(
      mergeRequests.map(async (mr) => {
        return await api.MergeRequestDiscussions.all(mr.project_id, mr.iid);
      })
    )
  ).forEach((commentsGroup) =>
    commentsGroup.forEach((comment) =>
      comment.notes.forEach((note) => mergeRequestsNotes.push(note))
    )
  );

  console.log("Finding all members approval actions...");
  for (const member of TCMembers) {
    const memberNotes = mergeRequestsNotes.filter(
      (note) => note.author.name === member
    );

    const memberApprovalsCount = memberNotes.filter(
      (note) => note.body === "approved this merge request"
    ).length;

    const memberCommentsCount = memberNotes.filter(
      (note) => note.type === "DiscussionNote"
    ).length;

    result[member].comments = memberCommentsCount;
    result[member].approvals = memberApprovalsCount;
  }
};
