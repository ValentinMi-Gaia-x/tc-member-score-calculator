import { Gitlab } from "@gitbeaker/node";
import * as dotenv from "dotenv";
dotenv.config();

export const api = new Gitlab({
  host: process.env.GITLAB_HOST,
  token: process.env.GITLAB_TOKEN,
});
